#include "templateparser.h"
#include <console.h>
#include <iostream>
#include <QRegularExpression>
#include <QFileInfo>

using namespace std;

/**
 * @brief
 *
 * The constructor of the class. It initializes variables and try to open files
 *
 * @param TemplateFileName      Name of the file with template
 * @param OutputFileName        Name of the output file, where template will be parsed
 * @param Variables             List of the variables to replace in the template
 */
TemplateParser::TemplateParser(QString TemplateFileName , QString OutputFileName, QMap<QString,QString> Variables )
{
    // Store variables
    cv_Variables            = Variables;

    // Preparing files
    cv_TemplateFile         = new QFile( TemplateFileName );
    cv_OutputFile           = new QFile( OutputFileName );

    // Checking if template file exists
    if ( !cv_TemplateFile->open ( QFile::ReadOnly ) )
    {
        PrintError( QString("Cannot open a template file named: %1").arg(TemplateFileName).toStdString() );
        return;
    }

    // Checking if can create output file
    if ( !cv_OutputFile->open ( QFile::WriteOnly ) )
    {
        PrintError( QString("Cannot create a output file named: %1").arg(TemplateFileName).toStdString() );
        return;
    }

    // Preparing text stream for files
    cv_TemplateStream       = new QTextStream( cv_TemplateFile );

    // Preparing file info for variables
    QFileInfo TemplateFileInfo( *cv_TemplateFile );
    QFileInfo OutputFileInfo( *cv_OutputFile );

    // Putting standard variables to the list
    cv_Variables[ DATE_VARIABLE ]               = QDate::currentDate().toString("yyyy-MM-dd");
    cv_Variables[ YEAR_VARIABLE ]               = QDate::currentDate().toString("yyyy");
    cv_Variables[ TIME_VARIABLE ]               = QTime::currentTime().toString("HH:mm:ss");
    cv_Variables[ TMPLPARS_VERSION_VARIABLE ]   = VERSION;
    cv_Variables[ TEMPLATE_FILE_NAME_VARIABLE ] = TemplateFileInfo.fileName();
    cv_Variables[ OUTPUT_FILE_NAME_VARIABLE ]   = OutputFileInfo.fileName();

    // Parsing a file
    Parse();
}

/**
 * @brief
 *
 * The function parse a file and print result to the output file. It is main parsing function.
 */
void TemplateParser::Parse()
{
    cv_LineCounter  = 0;

    while( ParseNextLine() );

    QTextStream OutputStream( cv_OutputFile );

    OutputStream << cv_OutputString;
}

/**
 * @brief
 *
 * The function read next line from template and return in
 *
 * @return new line from template
 */
QString TemplateParser::GetNextLineFromTemplate()
{
    cv_LineCounter++;
    return cv_TemplateStream->readLine();
}

/**
 * @brief
 *
 * The function parses a next line in the file
 *
 * @return true if file is not finished
 */
bool TemplateParser::ParseNextLine()
{
    // Read next line
    QString Line                = GetNextLineFromTemplate();

    // Return result of new line execution
    return ParseAndExecuteLine(Line);
}

/**
 * @brief
 *
 * The function parse and execute line given as a parameter
 *
 * @param Line      The line to parse
 * @param Print     If the line should be printed
 * @return
 */
bool TemplateParser::ParseAndExecuteLine(QString Line , bool Print)
{
    // Parse instruction of the line
    Instruction_t Instruction       = ParseInstruction( Line );

    if ( ! ExecuteInstruction( Instruction , Print ) )
    {
        Line = PushVariables( Line );

        if ( Print )
        {
            cv_OutputString.push_back( Line );
            cv_OutputString.push_back("\n");
        }
    }

    return !cv_TemplateStream->atEnd();
}

/**
 * @brief
 *
 * The function read instruction from the line, and parameters of it (if exists)
 *
 * @param Line      current line to parse
 *
 * @return Instruction if exists i4n parsed line
 */
TemplateParser::Instruction_t TemplateParser::ParseInstruction(QString & Line)
{
    // Create a variable to return
    Instruction_t Instruction;

    ///////////////////////////////////////////////
    ///
    /// Read a tag and parameter of a instruction
    ///

    // Create a regular expresion object (static to do it only once)
    static QRegularExpression RegExp;

    // Set the pattern with instruction with parameters
    RegExp.setPattern( INSTRUCTION_WITH_PARAMETER_REG_EXP );

    // Set the subject and match the expresion
    QRegularExpressionMatch match = RegExp.match( Line );

    // Checking if line contains a instruction with parameter
    if ( match.hasMatch() )
    {
        // The line contains instruction with parameter
        Instruction.Tag         = match.captured(1);
        Instruction.Parameters  = match.captured(2);
    }
    else
    {
        // Set the pattern for instruction without parameters
        RegExp.setPattern( INSTRUCTION_WITHOUT_PARAMETER_REG_EXP );

        // Match the pattern of instruction without parameter
        match   = RegExp.match( Line );

        // Check if line contains instruction without parameters
        if ( match.hasMatch() )
        {
            // The line contains instruction without parameter
            Instruction.Tag         = match.captured(1);
        }
    }

    return Instruction;
}

/**
 * @brief
 *
 * The function execute a instruction if it exists
 *
 * @param Instruction       Instruction to execute
 * @param Print     If the line should be printed
 *
 * @return true if instruction was executed, false if instruction not exists
 */
bool TemplateParser::ExecuteInstruction(Instruction_t &Instruction , bool Print)
{
    bool InstructionExists = true;

    if ( Instruction.Tag == IF_TAG )
    {
        ExecuteIF( Instruction , Print);
    }
    else if ( Instruction.Tag == ELSE_IF_TAG )
    {
        PrintWarning( QString("%1:%2 - Warning! The ELSE_IF tag without a previous IF!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
    }
    else if ( Instruction.Tag == ELSE_TAG )
    {
        PrintWarning( QString("%1:%2 - Warning! The ELSE tag without a previous IF!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
    }
    else if ( Instruction.Tag == END_IF_TAG )
    {
        PrintWarning( QString("%1:%2 - Warning! The END_IF tag without a previous IF!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
    }
    else if ( Instruction.Tag == REPEAT_TAG )
    {
        ExecuteREPEAT( Instruction , Print);
    }
    else if ( Instruction.Tag == END_REPEAT_TAG )
    {
        PrintWarning( QString("%1:%2 - Warning! The END_REPEAT tag without a previous REPEAT!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
    }
    else if ( Instruction.Tag == SET_TAG )
    {
        ExecuteSET( Instruction , Print);
    }
    else
    {
        InstructionExists   = false;
    }

    return InstructionExists;
}

/**
 * @brief
 *
 * The function execute a IF block
 *
 * @param Instruction       a structure with the IF parameters
 * @param Print             If the line should be printed
 */
void TemplateParser::ExecuteIF(Instruction_t &Instruction , bool Print)
{
    int conditionResult         = ParseCondition( Instruction.Parameters );

    // Create a flag if tag was found
    bool tagFound               = false;
    bool notFinishedBlock       = false;

    // Create list of possible tags
    QStringList possibleTags;

    // Save line number for the case if next instruction will be not found
    int StartLineNumber     = cv_LineCounter;

    possibleTags << ELSE_IF_TAG;
    possibleTags << ELSE_TAG;
    possibleTags << END_IF_TAG;

    if ( conditionResult < 0 )
    {
        //////////////////////////////////////////////
        // Parameters not given
        PrintError( QString("%1:%2 - Warning! The IF tag require a parameter! help: <IF> ( parameters )").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
    }
    else if ( conditionResult == 1 )
    {
        //////////////////////////////////////////////
        // Paramerers given and condition is true

        // Parse everything to the next possible tag
        Instruction_t NextInstruction   = ParseToTags( possibleTags  , &tagFound , Print);

        if ( tagFound && (NextInstruction.Tag != END_IF_TAG) )
        {
            // It is needed to create new list of the possible tags
            possibleTags.clear();

            // Now only possible tag is the end of the 'IF' block
            possibleTags << END_IF_TAG;

            // Skip all to end of the block
            NextInstruction     = ParseToTags( possibleTags , &tagFound , false);

        }

        // Save if that block has end tag
        notFinishedBlock    = !tagFound;
    }
    else
    {
        //////////////////////////////////////////////
        // Parameters given but condition is false


        // Skip this block
        Instruction_t NextInstruction   = ParseToTags( possibleTags , &tagFound  , false);

        // Check if next instruction found
        if ( tagFound )
        {
            if ( NextInstruction.Tag == ELSE_IF_TAG )
            {
                ExecuteIF( NextInstruction , Print);
            }
            else if ( NextInstruction.Tag == ELSE_TAG )
            {
                possibleTags.clear();

                possibleTags    << END_IF_TAG;

                NextInstruction = ParseToTags( possibleTags , &tagFound , Print);

            }
        }
        notFinishedBlock    = !tagFound;
    }

    if ( notFinishedBlock )
    {
        PrintWarning( QString("%1:%2 - Warning! The IF tag has not end!").arg( cv_TemplateFile->fileName() ).arg(StartLineNumber).toStdString() );
    }
}

/**
 * @brief
 *
 * The function execute a REPEAT block
 *
 * @param Instruction       Last instruction to execute
 * @param Print     If the line should be printed
 */
void TemplateParser::ExecuteREPEAT(Instruction_t &Instruction , bool Print)
{
    bool canConvert;
    int N   = ParseValue( Instruction.Parameters ).toInt(&canConvert , 0);

    if( N == 0 || canConvert == false)
    {
        QStringList possibleTags;
        bool        found;

        possibleTags << END_REPEAT_TAG;

        ParseToTags(possibleTags,&found,false);
    }

    if ( canConvert )
    {
        // Correctly converted to a number of repeats

        qint64 startPos     = cv_TemplateStream->pos();
        int startLine       = cv_LineCounter;

        // Repeat it N times
        for( int i = 0 ; i < N ; i++)
        {
            // A flag if end tag was found
            bool tagFound = false;

            cv_TemplateStream->seek(startPos);
            cv_LineCounter  = startLine;

            QString savedRepeatIndex            = cv_Variables[REPEAT_INDEX_VARIABLE];
            cv_Variables[REPEAT_INDEX_VARIABLE] = QString::number(i);

            do
            {
                QString line    = GetNextLineFromTemplate();

                Instruction_t nextInstruction = ParseInstruction( line );

                if ( nextInstruction.Tag == END_REPEAT_TAG )
                {
                    tagFound    = true;
                }
                else
                {
                    ParseAndExecuteLine(line , Print);
                }
            } while( !cv_TemplateStream->atEnd() && !tagFound  );

            cv_Variables[REPEAT_INDEX_VARIABLE] = savedRepeatIndex;

            if ( !tagFound )
            {
                PrintWarning( QString("%1:%2 - Warning! The REPEAT tag has not end!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).toStdString() );
            }
        }
    }
    else
    {
        // Cannot convert to a counter
        PrintError( QString("%1:%2 - Error! Cannot read a number of repeats from '%3'").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(Instruction.Parameters).toStdString() );
    }
}

/**
 * @brief
 *
 * The function execute SET instruction
 *
 * @param Instruction
 * @param Print     If the line should be printed
 */
void TemplateParser::ExecuteSET(Instruction_t &Instruction, bool Print)
{
    bool result = true;
    QRegularExpression RegExp( SET_VARIABLE_TO_VALUE_REG_EXP );

    QRegularExpressionMatch match = RegExp.match( Instruction.Parameters );

    QString VariableToSet;
    QString Value;

    if ( match.hasMatch() )
    {
        // Simple assigment of the value
        VariableToSet   = match.captured(1);
        Value           = match.captured(2);
    }
    else
    {
        RegExp.setPattern( SET_VARIABLE_TO_OTHER_VARIABLE_REG_EXP );

        match = RegExp.match( Instruction.Parameters );

        if ( match.hasMatch() )
        {
            // assigment of a variable
            VariableToSet           = match.captured(1);
            QString OtherVariable   = match.captured(2);

            if ( cv_Variables.contains( OtherVariable ) )
            {
                Value   = cv_Variables[OtherVariable];
            }
            else
            {
                // Cannot find second variable
                PrintError( QString("%1:%2 - Error! Cannot SET variable %4 to variable '%3', because it not exists!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(OtherVariable).arg(VariableToSet).toStdString() );
                result  = false;
            }
        }
        else
        {
            // Cannot find variable to set
            PrintError( QString("%1:%2 - Error! Parameter '%3' of the SET instruction is not correct!").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(Instruction.Parameters).toStdString() );
            result  = false;
        }
    }

    if ( result && Print)
    {
        cv_Variables[VariableToSet] = Value;
    }
}

/**
 * @brief
 *
 * The function push variables from a list to the line
 *
 * @param Line
 * @return A line with pushed variables
 */
QString TemplateParser::PushVariables(QString & Line)
{
    ////////////////////////////////////////////////////////////
    ///                   SIMPLE VARIABLES
    ////////////////////////////////////////////////////////////
    {
        // Create regular expression for reading a name of the tag
        QRegularExpression RegExp( VARIABLE_REG_EXP );

        // Creating iterator to find all occurrence of the variables
        QRegularExpressionMatchIterator i = RegExp.globalMatch( Line );

        // While next match exists
        while( i.hasNext() )
        {
            // Get next match
            QRegularExpressionMatch match = i.next();

            if ( match.hasMatch() )
            {
                // The line contains a variable
                QString VariableTag     = match.captured(1);

                if ( cv_Variables.contains( VariableTag ) )
                {
                    // The variable tag is on the variables list, so replace the occurrence of it
                    Line.replace( QString("[%1]").arg(VariableTag) , cv_Variables.value(VariableTag) );
                }
                else
                {
                    // The tag is not on the list
                    PrintWarning( QString("%1:%2 - Warning! The tag [%3] is not recognized").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(VariableTag).toStdString() );
                }
            }
        }
    }

    ////////////////////////////////////////////////////////////
    ///                   ARRAY VARIABLES
    ////////////////////////////////////////////////////////////
    {
        // Create regular expression for reading a name of the tag
        QRegularExpression RegExp( ARRAY_VARIABLE_REG_EXP );

        // Creating iterator to find all occurrence of the variables
        QRegularExpressionMatchIterator i = RegExp.globalMatch( Line );

        // While next match exists
        while( i.hasNext() )
        {
            // Get next match
            QRegularExpressionMatch match = i.next();

            if ( match.hasMatch() )
            {
                // The line contains a variable
                QString VariableTag     = match.captured(1);
                QString IndexTag        = match.captured(2);
                QString VariableName    = QString("%1:%2").arg(VariableTag).arg(IndexTag);

                if ( cv_Variables.contains( VariableName ) )
                {
                    // The variable tag is on the variables list, so replace the occurrence of it
                    Line.replace( QString("[%1:%2]").arg(VariableTag).arg(IndexTag) , cv_Variables.value(VariableName) );
                }
                else
                {
                    // The tag is not on the list
                    PrintWarning( QString("%1:%2 - Warning! The tag [%3:%4] is not recognized").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(VariableTag).arg(IndexTag).toStdString() );
                }
            }
        }
    }

    return Line;
}

/**
 * @brief
 *
 * The function parses a condition given as a parameter
 *
 * @param condition
 * @return 0 if condition correct, but false, 1 if condition correct and true, -1 if error
 */
int TemplateParser::ParseCondition(QString Condition)
{
    // Preparation of the return result
    int result = -1;

    // Prepare a regular expression
    QRegularExpression RegExp( CONDITIONS_REG_EXP );

    // Parse all conditions
    QRegularExpressionMatchIterator i = RegExp.globalMatch( Condition );

    // Searching conditions
    while( i.hasNext() && (result != 0) )
    {
        QRegularExpressionMatch match = i.next();

        QString  leftString          = match.captured(1);
        QString  comparisonString    = match.captured(2);
        QString  rightString         = match.captured(3);
        QString  logicString         = match.captured(4);

        QVariant leftValue           = ReadValue( ParseValue( leftString ) );
        QVariant rightValue          = ReadValue( ParseValue( rightString ) );

        bool currentResult           = CheckCondition( leftValue , comparisonString , rightValue );

        if ( logicString == "||" )
        {
            result = (currentResult || (result == 1) ) ? 1 : 0;
        }
        else if ( logicString == "&&" )
        {
            result = (currentResult && (result == 1) ) ? 1 : 0;
        }
        else if ( i.hasNext() )
        {
            PrintError( QString("%1:%2 - Error! Cannot recognize a logic operator: '%3'").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(logicString).toStdString() );
        }
        else
        {
            result  = (currentResult) ? 1 : 0;
        }
    }

    return result;
}

/**
 * @brief
 *
 * The function parses param string to check if there is a tag, and convert it if true.
 *
 * @param Param         The param to convert to a value
 *
 * @return value of the variable if
 */
QString TemplateParser::ParseValue(QString Param)
{
    QString            result = Param;
    QRegularExpression RegExp( VARIABLE_REG_EXP );
    QString            paramAfterReplace = Param;

    QRegularExpressionMatch match = RegExp.match( Param );

    if ( match.hasMatch() )
    {
        QString tag     = match.captured(1);

        if ( cv_Variables.contains( tag ) )
        {
            // Tag found
            result = cv_Variables.value(tag);
            paramAfterReplace.replace(match.captured(0),result);
        }
        else
        {
            // Tag not recognized
            PrintWarning( QString("%1:%2 - Warning! The tag [%3] is not recognized").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(tag).toStdString() );
            result = Param;
        }
    }
    else
    {
        QRegularExpression RegExp( ARRAY_VARIABLE_REG_EXP );

        QRegularExpressionMatch match = RegExp.match( Param );

        if ( match.hasMatch() )
        {
            QString tag          = match.captured(1);
            QString indexTag     = match.captured(2);
            QString variableName = QString("%1:%2").arg(tag).arg(indexTag);

            if ( cv_Variables.contains( variableName ) )
            {
                // Tag found
                result = cv_Variables.value(variableName);
                paramAfterReplace.replace(QString("[%1]").arg(variableName),result);
            }
            else
            {
                // Tag not recognized
                PrintWarning( QString("%1:%2 - Warning! The tag [%3:%4] is not recognized").arg( cv_TemplateFile->fileName() ).arg(cv_LineCounter).arg(tag).arg(indexTag).toStdString() );
                result = Param;
            }
        }
        result = Param;
    }

    if(paramAfterReplace != Param)
    {
        result = ParseValue(paramAfterReplace);
    }

    return result;
}

/**
 * @brief
 *
 * The function converts value from a string and return it as a QVariant. It NOT recognize a tags!
 *
 * @param ValueString
 * @return
 */
QVariant TemplateParser::ReadValue(QString ValueString)
{
    QVariant Value;
    bool canConvert;

    // Try to convert to int
    Value = ValueString.toInt( &canConvert , 0 );

    if ( canConvert )
    {
        return Value;
    }

    // Try to convert it to double
    Value = ValueString.toDouble(&canConvert);

    if ( canConvert )
    {
        return Value;
    }

    // Check if can be convert to true or false
    if ( ValueString.toLower() == "true" )
    {
        return true;
    }

    if ( ValueString.toLower() == "false" )
    {
        return false;
    }



    // Cannot convert it, it is a string
    Value   = ValueString;

    return Value;
}

/**
 * @brief
 *
 * The function checks if condition is met
 *
 * @param Left              Left variable
 * @param Comparison        Comparison
 * @param Right             Right variable
 * @return true if condition is met
 */
bool TemplateParser::CheckCondition(QVariant Left, QString Comparison, QVariant Right)
{
    bool result = false;

    if ( Comparison == "==" )
    {
        result = Left == Right;
    }
    else if ( Comparison == ">=" )
    {
        result = Left >= Right;
    }
    else if ( Comparison == "<=" )
    {
        result = Left <= Right;
    }
    else if ( Comparison == ">" )
    {
        result = Left > Right;
    }
    else if ( Comparison == "<" )
    {
        result = Left < Right;
    }
    else
    {
        result = Left.toBool();
    }

    return result;
}

/**
 * @brief
 *
 * The function read a template and search one of a tag from the list
 *
 * @param Tags       The tag to find
 * @param TagFount   The reference to the flag, if tag was founded
 *
 * @return true if tag found, false if end of the file
 */
TemplateParser::Instruction_t TemplateParser::ReadToTags(QStringList Tags , bool * TagFound)
{
    bool tagFound = false;
    Instruction_t Instruction;

    // Read a lines to find a tag
    while( !tagFound && !cv_TemplateStream->atEnd() )
    {
        // Read next line from the template
        QString Line = GetNextLineFromTemplate();

        // Read instruction
        Instruction = ParseInstruction( Line );

        // Check if it is
        if ( Tags.contains( Instruction.Tag ) )
        {
            tagFound    = true;
        }
    }

    // Save a status of the tag found
    if ( TagFound != 0 )
    {
        *TagFound   = tagFound;
    }

    return Instruction;
}

/**
 * @brief
 *
 * The function parse a template and search one of a tag from the list
 *
 * @param Tags       The tag to find
 * @param TagFount   The reference to the flag, if tag was founded
 * @param Print      If the line should be printed
 *
 * @return true if tag found, false if end of the file
 */
TemplateParser::Instruction_t TemplateParser::ParseToTags(QStringList Tags , bool * TagFound , bool Print)
{
    bool tagFound = false;
    Instruction_t Instruction;

    // Read a lines to find a tag
    while( !tagFound && !cv_TemplateStream->atEnd() )
    {
        // Read next line from the template
        QString Line = GetNextLineFromTemplate();

        // Read instruction
        Instruction = ParseInstruction( Line );

        // Check if it is
        if ( Tags.contains( Instruction.Tag ) )
        {
            tagFound    = true;
        }
        else
        {
            ParseAndExecuteLine( Line , Print);
        }
    }

    // Save a status of the tag found
    if ( TagFound != 0 )
    {
        *TagFound   = tagFound;
    }

    return Instruction;
}
