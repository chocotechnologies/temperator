#ifndef TEMPLATEPARSER_H
#define TEMPLATEPARSER_H

#include <QString>
#include <QStringList>
#include <QMap>
#include <QFile>
#include <QTextStream>
#include <QVariant>
#include <QDate>
#include <QTime>

// List of instructions
#define IF_TAG              "IF"
#define ELSE_IF_TAG         "ELSE_IF"
#define ELSE_TAG            "ELSE"
#define END_IF_TAG          "END_IF"

#define REPEAT_TAG          "REPEAT"
#define END_REPEAT_TAG      "END_REPEAT"

#define SET_TAG             "SET"

#define INSTRUCTION_WITH_PARAMETER_REG_EXP      "<(\\w+)>\\s*\\(([^\\)]*)\\)"
#define INSTRUCTION_WITHOUT_PARAMETER_REG_EXP   "<(\\w+)>"
#define VARIABLE_REG_EXP                        "\\[(\\w+)\\]"
#define ARRAY_VARIABLE_REG_EXP                  "\\[(\\w+)\\:(\\w+)\\]"
#define CONDITIONS_REG_EXP                      "(\\[?\\w+\\]?)\\s*([=<>]*)\\s*(\\[?\\w*\\]?)\\s*([|&]*)"
#define SET_VARIABLE_TO_VALUE_REG_EXP           "\\[(\\w+)\\]\\s*[=]\\s*(\\w+)"
#define SET_VARIABLE_TO_OTHER_VARIABLE_REG_EXP  "\\[(\\w+)\\]\\s*[=]\\s*\\[(\\w+)\\]"

#define REPEAT_INDEX_VARIABLE       "REPEAT_INDEX"
#define DATE_VARIABLE               "DATE"
#define YEAR_VARIABLE               "YEAR"
#define TIME_VARIABLE               "TIME"
#define TEMPLATE_FILE_NAME_VARIABLE "TEMPLATE_FILE_NAME"
#define OUTPUT_FILE_NAME_VARIABLE   "OUTPUT_FILE_NAME"
#define TMPLPARS_VERSION_VARIABLE   "TMPLPARS_VERSION"

#define VERSION                     "TmplPars V1.1.0"

/**
 * @brief
 *
 * The TemplateParser class is responsible for parsing a template file
 */
class TemplateParser
{
    // Types
    private:

        /**
         * @brief
         *
         * The type for storing information about current instruction
         */
        struct Instruction_t
        {
            QString                 Tag;            /**< Tag of the instruction */
            QString                 Parameters;     /**< Parameters of the instruction */
        };

    // Variables
    private:
        QMap<QString,QString>   cv_Variables;       /**< List of variables available in the program */
        QFile                  *cv_TemplateFile;    /**< File of the template */
        QFile                  *cv_OutputFile;      /**< Output file */
        QTextStream            *cv_TemplateStream;  /**< Stream of the template */
        QString                 cv_OutputString;    /**< String with the output */
        int                     cv_LineCounter;     /**< Current line counter */

    // Methods
    private:

        void                Parse();
        QString             GetNextLineFromTemplate();
        bool                ParseNextLine();
        bool                ParseAndExecuteLine( QString Line , bool Print = true );
        Instruction_t       ParseInstruction(QString & Line );
        bool                ExecuteInstruction( Instruction_t & Instruction , bool Print = true);
        void                ExecuteIF( Instruction_t &Instruction , bool Print = true);
        void                ExecuteREPEAT(Instruction_t &Instruction , bool Print = true);
        void                ExecuteSET( Instruction_t &Instruction , bool Print = true);
        QString             PushVariables(QString & Line );
        int                 ParseCondition( QString Condition );
        QString             ParseValue( QString Param );
        QVariant            ReadValue( QString ValueString );
        bool                CheckCondition( QVariant Left , QString Comparison , QVariant Right );
        Instruction_t       ReadToTags(QStringList Tags , bool * TagFound = 0);
        Instruction_t       ParseToTags(QStringList Tags , bool * TagFound = 0 , bool Print = true);

    // Methods
    public:

        TemplateParser( QString TemplateFileName , QString OutputFileName , QMap<QString,QString> Variables );
};

#endif // TEMPLATEPARSER_H
